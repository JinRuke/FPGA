module rst_gen(
	input	clk,
	output reg	rst_n
	);
	reg	[15:0] cnt = 0;
always@(posedge clk)
	begin
		if(cnt==16'h1ff)
			;
		else
			cnt<=cnt+1;
	end
always@(posedge clk)
	begin
		if(cnt==16'h1ff)
			rst_n<=1;
		else
			rst_n<=0;
	end
endmodule