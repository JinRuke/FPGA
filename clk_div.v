module clk_div(
	input	clk,
	input	rst_n,
	output	reg	clk_out
	);
	reg	[15:0]	cnt;
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				cnt<=0;
			else if(cnt==2)
				cnt<=0;
			else
				cnt<=cnt+1;
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				clk_out<=0;
			else //if(cnt==2)
				clk_out<=~clk_out;
			//else
			//	;
		end
	endmodule