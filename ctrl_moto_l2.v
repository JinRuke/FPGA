module ctrl_moto_l2(
	input					clk									,//50M
	input					rst_n								,
	input					signal_in				,//高电平有效
	output	reg		step_out						
	);
				
	
	parameter	idle										= 8'h0,
						step_high 							= 8'h1,
						step_low  							= 8'h2;
						
	reg	[7:0]		curr_st				;
	reg	[7:0]		curr_st_ff1		;
	reg	[10:0]	step_high_time	;
	reg	[10:0]	step_low_time	;
	reg	[10:0]	step_high_cnt	;
	reg	[10:0]	step_low_cnt	;
	reg					sdcard_check_ff1;
	reg					sdcard_check_ff2;
	reg					sdcard_check_ff3;
	wire[10:0]	spd_high_time;
	wire[10:0]	spd_low_time ;
	wire[10:0]	hspd_high_time;
	wire[10:0]	hspd_low_time ;
	assign	spd_high_time=500;
	assign	spd_low_time =500;
	assign	hspd_high_time=2500;
	assign	hspd_low_time=2500;
	always@(posedge clk)sdcard_check_ff1<=signal_in;
	always@(posedge clk)sdcard_check_ff2<=sdcard_check_ff1;
	always@(posedge clk)sdcard_check_ff3<=sdcard_check_ff2;
	assign	sdcard_check_rise=sdcard_check_ff2&&(sdcard_check_ff3==0);
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				begin
					step_high_time<=0;
					step_low_time<=0;
				end
			/*else if(sdcard_check_ff3)
				begin
					step_high_time<=hspd_high_time;
					step_low_time<=hspd_low_time;
				end*/
			else
				begin
					step_high_time<=spd_high_time;
					step_low_time<=spd_low_time;
				end
		end
	always@(posedge clk)curr_st_ff1<=curr_st	;
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				curr_st<=idle;
			else if(sdcard_check_rise)
				curr_st<=idle;
			else case(curr_st)
				idle:curr_st<=step_high	;
				step_high:
					begin
						if(step_high_cnt==step_high_time)
							curr_st<=step_low;
						else
							;
					end
				step_low:
					begin
						if(step_low_cnt==step_low_time)
							curr_st<=step_high;
						else
							;
					end
				default:;
			endcase
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				step_high_cnt<=0;
			else if(curr_st==idle)
				step_high_cnt<=0;
			else if(curr_st==step_high)
				step_high_cnt<=step_high_cnt+1;
			else
				step_high_cnt<=0;
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				step_low_cnt<=0;
			else if(curr_st==idle)
				step_low_cnt<=0;
			else if(curr_st==step_low)
				step_low_cnt<=step_low_cnt+1;
			else
				step_low_cnt<=0;
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				begin
					step_out<=0;
				end
			else if(curr_st==idle)
				begin
					step_out<=0;
				end
			else if(sdcard_check_ff3==1)
				step_out<=1;
			else if(curr_st==step_high)
				step_out<=1;
			else if(curr_st==step_low)
				step_out<=0;
			else
				;
		end
endmodule