module car_top(
	input	clk,
	input	key_start	,//启动按键
	input	sensor_in_l,//左边灭火传感器，低电平有效
	input	sensor_in_r,//右边灭火传感器，低电平有效
	input	sensor_in_m,//中间灭火传感器，低电平有效
	
	output	in1_l,//左边电机方向标志
	output	in2_l,//左边电机方向标志
	output	pwm_l,//左边电机速度控制
	
	output	in1_r,//右边电机方向标志
	output	in2_r,//右边电机方向标志
	output	pwm_r,//右边电机速度控制
	
	
	output  miehuo_en//灭火，低电平风扇开启
	
	);
	reg	sensor_in_l_ff1	;
	reg	sensor_in_l_ff2 ;
	reg	sensor_in_m_ff1 ;
	reg	sensor_in_m_ff2 ;
	reg	sensor_in_r_ff1 ;
	reg	sensor_in_r_ff2 ;

	assign	pwm_l=pwm;
	assign	pwm_r=pwm;
	always@(posedge clk)sensor_in_l_ff1<=sensor_in_l;
	always@(posedge clk)sensor_in_l_ff2<=sensor_in_l_ff1;
	always@(posedge clk)sensor_in_m_ff1<=sensor_in_m;
	always@(posedge clk)sensor_in_m_ff2<=sensor_in_m_ff1;
	always@(posedge clk)sensor_in_r_ff1<=sensor_in_r;
	always@(posedge clk)sensor_in_r_ff2<=sensor_in_r_ff1;
	//复位信号生成模块
	rst_gen Urst_gen(
		.clk		(clk				),
		.rst_n		(rst_n				)
	);
	//按键消抖模块
	key_xd Ukey_xd(
	.clk			(clk),
	.rst_n			(rst_n),
	.key_in			(key_start),
	.key_out		(key_start_out )
		);
	//传感器输入信号消抖
	signal_xd Usensor_in_l(
	.clk			(clk),
	.rst_n			(rst_n),
	.key_in			(sensor_in_l_ff2),
	.key_out		(sensor_in_l_ff2_out)
		);
	//传感器输入信号消抖
	signal_xd Usensor_in_m(
	.clk			(clk),
	.rst_n			(rst_n),
	.key_in			(sensor_in_m_ff2),
	.key_out		(sensor_in_m_ff2_out)
		);
	//传感器输入信号消抖
	signal_xd Usensor_in_r(
	.clk			(clk),
	.rst_n			(rst_n),
	.key_in			(sensor_in_r_ff2),
	.key_out		(sensor_in_r_ff2_out)
		);
	//电机速度控制模块
	ctrl_moto_pwm Uctrl_moto_pwm(
	.clk			(clk),//50M
	.rst_n			(rst_n),
	.pwm			(pwm)			
	);
	//电机方向控制模块
	ctrl_moto_dir Uctrl_moto_dir(
	.clk			(clk),
	.rst_n			(rst_n),
	.start			(key_start_out),
	.sensor_l		(sensor_in_l_ff2_out),
	.sensor_m		(sensor_in_m_ff2_out),
	.sensor_r		(sensor_in_r_ff2_out),
	.in1_l			(in1_l),
	.in2_l			(in2_l),
	.in1_r			(in1_r),
	.in2_r			(in2_r),
	.miehuo_en		(miehuo_en)
	);
endmodule