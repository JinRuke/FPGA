module ctrl_moto_dir(
	input	clk,
	input	rst_n,
	input	start,
	input	sensor_l,
	input	sensor_m,
	input	sensor_r,
	output	reg	in1_l,
	output	reg	in2_l,
	output	reg	in1_r,
	output	reg	in2_r,
	output	reg	miehuo_en
	);
	parameter	IDLE			= 3'd0;
	parameter	FORWARD         = 3'd1;
	parameter	TURN_LEFT       = 3'd2;
	parameter	TURN_RIGHT      = 3'd3;
	parameter	TURN_MID        = 3'd4;
	parameter	FINI            = 3'd5;
	reg	[2:0]	curr_st;
	
	reg	[31:0]	pause_wait_cnt;
	reg	[31:0]	wait_cnt;
	reg			miehuo_start;
	reg	[1:0]	miehuo_st	;
	reg	[31:0]	miehuo_wait_cnt;
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				curr_st<=IDLE;
			else case(curr_st)
				IDLE:
					begin
						if(start)
							curr_st<=FORWARD;
						else
							;
					end
				FORWARD:
					begin
						if(sensor_l==0)
							curr_st<=TURN_LEFT;
						else if(sensor_r==0)
							curr_st<=TURN_RIGHT;
						else if(sensor_m==0)
							curr_st<=TURN_MID;
						else
							;
					end
				TURN_LEFT:
					begin
						if(sensor_l==1)
							curr_st<=FINI;
						else
							;
					end
				TURN_RIGHT:
					begin
						if(sensor_r==1)
							curr_st<=FINI;
						else
							;
					end
				TURN_MID:
					begin
						if(sensor_m==1)
							curr_st<=FINI;
						else
							;
					end
				FINI:curr_st<=FORWARD;
				default:;
			endcase
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				begin
					in1_l<=0;
					in2_l<=0;
					in1_r<=0;
					in2_r<=0;
					miehuo_start<=1'b0;
				end
			else if(curr_st==FINI)
				begin
					in1_l<=0;
					in2_l<=0;
					in1_r<=0;
					in2_r<=0;
					miehuo_start<=1'b0;
				end
			else if(curr_st==FORWARD)
				begin
					 in1_l<=1;
					 in2_l<=0;
					 in1_r<=0;
					 in2_r<=1;
					 miehuo_start<=0;
				end
			else if(curr_st==TURN_MID)
				begin
					 in1_l<=1;
					 in2_l<=0;
					 in1_r<=0;
					 in2_r<=1;
					 miehuo_start<=1;
				end
			else if(curr_st==TURN_LEFT)
				begin
					 in1_l<=0;
					 in2_l<=1;
					 in1_r<=0;
					 in2_r<=1;
					 miehuo_start<=1;
				end
			else if(curr_st==TURN_RIGHT)
				begin
					 in1_l<=1;
					 in2_l<=0;
					 in1_r<=1;
					 in2_r<=0;
					 miehuo_start<=1;
				end
			else
				;
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				miehuo_st<=0;
			else case(miehuo_st)
				0:
					begin
						if(miehuo_start)
							miehuo_st<=1;
						else
							;
					end
				1:
					begin
						if(miehuo_wait_cnt==250000000)//延时5S，确保火被灭了
							miehuo_st<=0;
						else
							;
					end
				default:;
			endcase
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				miehuo_wait_cnt<=0;
			else if(miehuo_st==1)
				miehuo_wait_cnt<=miehuo_wait_cnt+1;
			else
				miehuo_wait_cnt<=0;
		end
	always@(posedge clk or negedge rst_n)
		begin
			if(!rst_n)
				miehuo_en<=1'bz;
			else if(miehuo_st==1)
				miehuo_en<=1'b0;
			else
				miehuo_en<=1'bz;
		end
endmodule